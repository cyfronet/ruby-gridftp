# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruby-gridftp/version'

Gem::Specification.new do |spec|
  spec.name          = 'ruby-gridftp'
  spec.version       = GFTP::VERSION
  spec.authors       = ['m.pawlik', 't.gubala']
  spec.email         = ['m.pawlik@cyfronet.pl', 't.gubala@cyfronet.pl']
  spec.description   = %q{Ruby FFI-based gem that wraps Google Toolkit's GridFTP client library}
  spec.summary       = %q{Uses FFI mechanism to interface directly the globus-ftp-client.so shared library to deliver client side of GridFTP protocol}
  spec.homepage      = 'http://dev.cyfronet.pl/gitlab/m.pawlik/ruby-gridftp/'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'ffi', '~> 1.9', '>= 1.9.3'
  spec.add_runtime_dependency 'net-ssh'
  spec.add_runtime_dependency 'ed25519'
  spec.add_runtime_dependency 'bcrypt_pbkdf'
end
