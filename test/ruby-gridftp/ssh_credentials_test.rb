# frozen_string_literal: true

require "test_helper"

class GFTP::SshCredentialsTest < Minitest::Test
  def setup
    @credentials = GFTP::SshCredentials.new(fixture("key"), fixture("cert"))
  end

  def test_username_taken_from_certificate
    assert_equal "plgkasztelnik", @credentials.username
  end

  def test_with_stored_credentials
    invoked = false

    @credentials.with_stored_key_and_cert do |key_path, cert_path|
      assert_equal fixture("key"), File.read(key_path), "Wrong key payload in #{key_path}"
      assert_equal "600", mode(key_path), "Wrong key file permissions, should be 600"

      assert_equal fixture("cert"), File.read(cert_path), "Wrong certificate payload in #{cert_path}"
      assert_equal "#{key_path}-cert.pub", cert_path, "Wrong key-cert files naming"

      invoked = true
    end

    assert invoked, "block not invoked"
  end

  private
    def fixture(name)
      File.read(File.join("test/fixtures", name))
    end

    def mode(file_path)
      File.stat(file_path).mode.to_s(8).split("")[-3..-1].join
    end
end
