require 'ffi'
require_relative '../lib/ruby-gridftp/gftp'

URL = "gsiftp://qcg.grid.cyf-kr.edu.pl/"
BUF_SIZE = 500
ftphandle = FFI::MemoryPointer.new :pointer
ftphandleattr = FFI::MemoryPointer.new :pointer
operationattr = FFI::MemoryPointer.new :pointer
credentialid = FFI::MemoryPointer.new :pointer
gssbuf = GFTP::GSSBufferDesc.new
gssbufout = GFTP::GSSBufferDesc.new

minstat = FFI::MemoryPointer.new(:int)

$mutex = FFI::MemoryPointer.new(:int, 10)
$cond = FFI::MemoryPointer.new(:int, 14)
$done = false

#callbacks
GFTP::CompleteCallback = Proc.new do |user_arg, l_ftphandle, error|
  p "Done!"
  if error != nil
    p "Error:", GFTP.globus_object_printable_to_string(error).read_string
  end
  p GFTP.globus_mutex_lock($mutex)
  $done = true
  p GFTP.globus_cond_signal($cond)
  p GFTP.globus_mutex_unlock($mutex)
end

GFTP::DataCallback = Proc.new do |user_arg, l_ftphandle, error, buf, length, offset, eof|
  p "length: #{length}, offset #{offset}, eof #{eof}"
  #p "got data!", buf.read_string
  if eof != 1
    p GFTP.globus_ftp_client_register_read(l_ftphandle, buf, BUF_SIZE, GFTP::DataCallback, nil)
  end
end

p GFTP.globus_module_activate(GFTP.globus_i_ftp_client_module)
p GFTP.globus_ftp_client_handleattr_init(ftphandleattr)
p GFTP.globus_ftp_client_handleattr_set_rfc1738_url(ftphandleattr, 1)
p GFTP.globus_ftp_client_handle_init(ftphandle, ftphandleattr)

p GFTP.globus_ftp_client_operationattr_init(operationattr)

#proxy_location = FFI::MemoryPointer.from_string('X509_USER_PROXY=/home/yaq/proxy.pem')
#gssbuf[:value] = proxy_location
#gssbuf[:length] = proxy_location.read_string.length

#gssbuf[:value] = FFI::MemoryPointer.from_string File.read("/home/yaq/proxy.pem")
gssbuf[:value] = FFI::MemoryPointer.from_string File.read("/tmp/x509up_u1")
gssbuf[:length] = gssbuf[:value].read_string.length

gssbufout[:value] = FFI::MemoryPointer.new(:char, 1000)

p "ptr: #{credentialid.read_int}"
p GFTP.gss_import_cred(minstat, credentialid, 0, 0, gssbuf, 0, nil)
p "minstat: #{minstat.read_int}"
p "ptr: #{credentialid.read_int}"
#p GFTP.gss_export_cred(minstat, credentialid.read_pointer, 0, 1, gssbufout)





p GFTP.globus_ftp_client_operationattr_set_authorization(operationattr, credentialid.read_pointer, nil, nil, nil, nil)

p GFTP.globus_mutex_init($mutex, nil)
p GFTP.globus_cond_init($cond, nil)


buf = FFI::MemoryPointer.new(:char, BUF_SIZE)
#p GFTP.globus_ftp_client_verbose_list(ftphandle, URL, nil, GFTP::CompleteCallback, nil)
p GFTP.globus_ftp_client_verbose_list(ftphandle, URL, operationattr, GFTP::CompleteCallback, nil)
p GFTP.globus_ftp_client_register_read(ftphandle, buf, BUF_SIZE, GFTP::DataCallback, nil)

p GFTP.globus_mutex_lock($mutex)
while $done != true
  p GFTP.globus_cond_wait($cond, $mutex)
end
p GFTP.globus_mutex_unlock($mutex)

p GFTP.globus_ftp_client_operationattr_destroy(operationattr)
p GFTP.globus_ftp_client_handleattr_destroy(ftphandleattr)
p GFTP.globus_ftp_client_handle_destroy(ftphandle)
p GFTP.globus_module_deactivate(GFTP.globus_i_ftp_client_module)