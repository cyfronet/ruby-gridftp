# encoding: UTF-8

#require 'spec_helper'

require_relative '../lib/ruby-gridftp'


module GFTP

  describe 'GFTP library' do

    let(:binary_test_file) { 'spec/boxplot_after_en.png' }
    let(:another_binary_test_file_1) { 'spec/boxplot_after_en_another_1.png' }
    let(:another_binary_test_file_2) { 'spec/boxplot_after_en_another_2.png' }
    let(:uid) { `id -u`.to_i }
    let(:proxy_file) { "/tmp/x509up_u#{uid}" }
    let(:dn) { `openssl x509 -noout -in #{proxy_file} -subject` }
    let(:is_tg) { File.exist?(proxy_file) and dn.include?('/C=PL/O=GRID/O=Cyfronet/CN=Tomasz Gubala') }

    let(:test_dir) { 'sftp://zeus.cyfronet.pl/mnt/gpfs/work/plgrid/groups/plggdata/.gridftp/' }
    let(:proxy_string) { File.read(['/home/yaq/proxy.pem', proxy_file].detect{|path| File.exists? path}) }


    context 'when loaded globus library' do
      it 'does not crash OpenSSL cipher method after Globus module deactivation' do
        require 'openssl'
        op = Operation.new(proxy: proxy_string)
        op.run_operation {|d| op.signal_completion}
        expect { OpenSSL::Cipher.new('aes-256-cbc') }.not_to raise_error
      end

      it 'returns GlobusError on wrong proxy string' do
        op = Operation.new(proxy: 'wrong_proxy_string')
        expect { op.run_operation {|d| op.signal_completion} }.to raise_error(GlobusError)
      end

      it 'is able to run several operations simultaneously' do
        op_outer = Operation.new(proxy: proxy_string)
        expect do
          op_outer.run_operation do |d1|
            op_inner = Operation.new(proxy: proxy_string)
            op_inner.run_operation {|d2| op_inner.signal_completion}
            op_outer.signal_completion
          end
        end.not_to raise_error
      end
    end


    context 'having to manage folder structure' do
      it 'is able to list a directory' do
        VerboseList.new(proxy: proxy_string).verbose_list test_dir do |data|
          expect(data.size).to be > 0
          expect(data.any?{|d| (d[:name] == 'testdir') and d[:is_dir]}).to eq true
          expect(data.any?{|d| (d[:name] == 'secret') and (d[:owner] == 'plgtesttg')}).to eq true
          expect(data.any?{|d| (d[:name] == 'secretdir') and (d[:group] == 'plggdata')}).to eq true
          expect(data.any?{|d| (d[:name] == 'boxplot_after_en.png') and !d[:is_dir]}).to eq true
          expect(data.any?{|d| (d[:name] == 'file with spaces.tsv') and !d[:is_dir]}).to eq true
        end
      end

      it 'is able to check existence of a file of a folder' do
        Exists.new(proxy: proxy_string).exists "#{test_dir}testdir" do |answer|
          expect(answer).to eq true
        end
        Exists.new proxy_string.exists "#{test_dir}boxplot_after_en.png" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}secret" do |answer|
          expect(answer).to eq true
        end
      end

      it 'is not able to check inexistent file or a file in unaccessible folder' do
        Exists.new(proxy: proxy_string).exists "#{test_dir}secretdir/secret" do |answer|
          expect(answer).to eq false
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}non-existent/" do |answer|
          expect(answer).to eq false
        end
      end

      context 'and create some' do
        it 'is able to create a folder on a remote server' do
          RmDir.new(proxy: proxy_string).rmdir!("#{test_dir}newdir") {}
          MkDir.new(proxy: proxy_string).mkdir "#{test_dir}newdir" do |answer|
            expect(answer).to eq true
          end
          Exists.new(proxy: proxy_string).exists "#{test_dir}newdir" do |answer|
            expect(answer).to eq true
          end
        end

        it 'is able chmod a folder to make it unaccessible' do
          MkDir.new(proxy: proxy_string).mkdir "#{test_dir}newdir/someplace" do |answer|
            expect(answer).to eq true
          end
          Chmod.new(proxy: proxy_string).chmod "#{test_dir}newdir", 0500 do |answer|
            expect(answer).to eq true
          end
          expect do
            RmDir.new(proxy: proxy_string).rmdir("#{test_dir}newdir/someplace")
          end.to raise_error
          Chmod.new(proxy: proxy_string).chmod "#{test_dir}newdir", 0770 do |answer|
            expect(answer).to eq true
          end
          RmDir.new(proxy: proxy_string).rmdir!("#{test_dir}newdir/someplace") do |answer|
            expect(answer).to eq true
          end
        end

        it 'is not able to re-create a folder when it exists' do
          Exists.new(proxy: proxy_string).exists "#{test_dir}newdir" do |answer|
            expect(answer).to eq true
          end
          expect do
            MkDir.new(proxy: proxy_string).mkdir "#{test_dir}newdir"
          end.to raise_error
        end
      end
    end


    context 'having to transfer some data from server' do
      it 'is able to get a file with precise length' do
        size = 0
        buf = ''
        Get.new(proxy: proxy_string).get "#{test_dir}uczacy.tsv" do |data|
          size += data.size
          buf = data
        end
        expect(size).to eq 1385
        expect(buf[-10..-1]).to eq "51325808\t1"
      end

      it 'is able to get a file with a space in its name' do
        size = 0
        Get.new(proxy: proxy_string).get "#{test_dir}file with spaces.tsv" do |data|
          size += data.size
        end
        expect(size).to eq 1385
      end

      it 'is able to get medium sized file from server without a Segfault' do
        size = 0
        buf = ''
        expect do
          Get.new(proxy: proxy_string).get "#{test_dir}revshseq_20-30_chr19" do |data|
            size += data.length
            buf = data
          end
        end.not_to raise_error
        expect(size).to eq 7662586
        expect(buf[-3..-1]).to eq "AG\n"
      end

      it 'is able to get binary file from server without mangling its contents' do
        size = 0
        tmp_bin_file = Tempfile.new('bindata')
        Get.new(proxy: proxy_string).get "#{test_dir}boxplot_after_en.png" do |data|
          size += data.size
          tmp_bin_file.write(data)
        end
        tmp_bin_file.close
        expect(FileUtils.cmp(tmp_bin_file.path, binary_test_file)).to eq true
        tmp_bin_file.unlink
      end
    end


    context 'having to perform somewhat more complex operations' do
      it 'is able to list a file and get it by its name' do
        VerboseList.new(proxy: proxy_string).verbose_list test_dir do |data|
          expect(data.size).to be > 0
          expect(data.any?{|d| (d[:name] == 'file with spaces.tsv') and !d[:is_dir]}).to eq true
          target_filename = data.detect{|d| d[:name] == 'file with spaces.tsv'}[:name]
          size = 0
          Get.new(proxy: proxy_string).get "#{test_dir}#{target_filename}" do |file_data|
            size += file_data.size
          end
          expect(size).to eq 1385
        end
      end
    end


    context 'having to transfer some data to the server' do
      it 'is able to put data as file on a remote server' do
        count = 0
        Put.new(proxy: proxy_string).put "#{test_dir}test.out" do |buf_size|
          data = "data #{count}\n"
          count += 1
          output = [ data, count > 7 ]
          output
        end
      end

      it 'is able to rename the remote file' do
        Move.new(proxy: proxy_string).move "#{test_dir}test.out", "#{test_dir}test_newname.out" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}test_newname.out" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}test.out" do |answer|
          expect(answer).to eq false
        end
      end

      it 'leaves the original file intact when not able to move the file elsewhere' do
        expect do
          Move.new(proxy: proxy_string).move "#{test_dir}test_newname.out", "#{test_dir}testdir/test.out"
        end.to raise_error
        Exists.new(proxy: proxy_string).exists "#{test_dir}test_newname.out" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}testdir/test.out" do |answer|
          expect(answer).to eq false
        end
      end

      it 'is able to move the file between folders' do
        MkDir.new(proxy: proxy_string).mkdir "#{test_dir}otherdir" do |answer|
          expect(answer).to eq true
        end
        Move.new(proxy: proxy_string).move "#{test_dir}test_newname.out", "#{test_dir}otherdir/test.out" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}test_newname.out" do |answer|
          expect(answer).to eq false
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}otherdir/test.out" do |answer|
          expect(answer).to eq true
        end
      end

      it 'is able to transfer the file to another folder' do
        Transfer.new(proxy: proxy_string).transfer "#{test_dir}test.out", "#{test_dir}copy" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}copy" do |answer|
          expect(answer).to eq true
        end
      end

      it 'is able to perform binary data round trip' do
        data_input = File.open binary_test_file
        test_file_name = "#{test_dir}test_binary.out"
        Put.new(proxy: proxy_string).put test_file_name do |buf_size|
          [ data_input.read(buf_size), data_input.eof? ]
        end
        data_input.close
        tmp_bin_file = Tempfile.new('bindata')
        Get.new(proxy: proxy_string).get test_file_name do |data|
          tmp_bin_file.write(data)
        end
        tmp_bin_file.close
        expect(FileUtils.cmp(tmp_bin_file.path, binary_test_file)).to eq true
        tmp_bin_file.unlink
        Delete.new(proxy: proxy_string).delete("#{test_dir}test_binary.out") {}
      end

      it 'is able to put data as file in several parts on a remote server' do
        offset = 0
        3.times do |part_no|
          data = "data part #{part_no}_"
          PartialPut.new(proxy: proxy_string).partial_put "#{test_dir}test_partial.out", data, offset
          offset += data.size
        end
        Delete.new(proxy: proxy_string).delete("#{test_dir}test_partial.out") {}
      end

      it 'is able to perform partial-based binary data round trip' do
        data_input = File.open binary_test_file
        test_file_name = "#{test_dir}test_binary_partial.out"
        offset = 0
        until data_input.eof?
          data = data_input.read(4096)
          PartialPut.new(4096, proxy: proxy_string).partial_put test_file_name, data, offset
          offset += data.size
          #puts "Sent #{data.size} data as a partial update to file"
        end
        data_input.close
        tmp_bin_file = Tempfile.new('bindata')
        Get.new(proxy: proxy_string).get test_file_name do |data|
          tmp_bin_file.write(data)
        end
        tmp_bin_file.close
        expect(FileUtils.cmp(tmp_bin_file.path, binary_test_file)).to eq true
        tmp_bin_file.unlink
        Delete.new(proxy: proxy_string).delete("#{test_dir}test_binary_partial.out") {}
      end

      it 'is able to work simultaneously' do
        def upload(source_filename)
          input_file = File.open source_filename
          output_filename = test_dir + '/' + File.basename(source_filename)
          Put.new(proxy: proxy_string).put output_filename do |buf_size|
            #puts "sending chunk of #{source_filename}"
            [input_file.read(buf_size), input_file.eof?]
          end
          input_file.close
        end

        t1 = Thread.new { upload another_binary_test_file_1; puts 'done1!' }
        t2 = Thread.new { upload another_binary_test_file_2; puts 'done2!' }
        t1.join
        t2.join
        Delete.new(proxy: proxy_string).delete("#{test_dir}/#{File.basename(another_binary_test_file_1)}") {}
        Delete.new(proxy: proxy_string).delete("#{test_dir}/#{File.basename(another_binary_test_file_2)}") {}
      end
    end


    context 'having to clean things up' do
      it 'is able to delete a file on remote server' do
        Delete.new(proxy: proxy_string).delete "#{test_dir}otherdir/test.out" do |answer|
          expect(answer).to eq true
        end
        Exists.new(proxy: proxy_string).exists "#{test_dir}test.out" do |answer|
          expect(answer).to eq false
        end
      end

      it 'is able to remove a folder on server' do
        RmDir.new(proxy: proxy_string).rmdir "#{test_dir}newdir" do |answer|
          expect(answer).to eq true
        end
        RmDir.new(proxy: proxy_string).rmdir "#{test_dir}otherdir" do |answer|
          expect(answer).to eq true
        end
      end
    end


    #context 'in a multi-user environment' do
    #  let(:other_proxy_string) { File.read(['/tmp/x509up_u_ttg'].detect{|path| File.exists? path}) }
    #
    #  it 'is able to handle multiple user proxy files' do
    #    e = Exists.new
    #    e.exists '/people/plgtesttg/', other_proxy_string do |answer|
    #      oe = Exists.new
    #      oe.exists '/people/ymgubala/', proxy_string do |oanswer|
    #        expect(oanswer).to be true
    #      end
    #      expect(answer).to be true
    #    end
    #  end
    #end

  end
end
