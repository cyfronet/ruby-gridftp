require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  #globus_result_t globus_ftp_client_move 	( 	globus_ftp_client_handle_t *  	u_handle,
  #   const char * 	source_url,
  #   const char * 	dest_url,
  #		globus_ftp_client_operationattr_t *  	attr,
  #		globus_ftp_client_complete_callback_t  	complete_callback,
  #		void *  	callback_arg
  #	)

  class Move < GFTP::Operation

    # Moves/renames the source_path folder or file into destination_path folder or file.
    # Uses proxy string for authentication and authorization.
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The operation is asynchronous; the passed block is being called with either true or false as the result
    # of the move operation..
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    def move(source_path, destination_path, &block)

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        # TODO Quite primitive check, but Globus do not leave too many options (globus_error_match didn't work for me)
        if (error != nil) and !(GFTP.globus_error_print_friendly(error).include? '550 550')
          raise_globus_error(error)
        else
          yield(error == nil)
        end
        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        source = URI.join(uri, source_path)
        destination = URI.join(uri, destination_path)
        res = GFTP.globus_ftp_client_move(ftphandle, source.to_s, destination.to_s, operationattr, @complete_callback, nil)
        check_error(res) if res != 0
      end
    end

  end
end
