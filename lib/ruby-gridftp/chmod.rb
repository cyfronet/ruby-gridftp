require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  #globus_result_t globus_ftp_client_chmod 	( 	globus_ftp_client_handle_t *  	u_handle,
  #		const char *  	url,
  #   int 	mode,
  #		globus_ftp_client_operationattr_t *  	attr,
  #		globus_ftp_client_complete_callback_t  	complete_callback,
  #		void *  	callback_arg
  #	)

  class Chmod < GFTP::Operation

    # Changes the path folder or file access attributes according to the mode octal number.
    # Uses proxy string for authentication and authorization.
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The operation is asynchronous; the passed block is being called with either true or false as the result
    # of the chmod operation..
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    def chmod(path, mode, &block)

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        # TODO Quite primitive check, but Globus do not leave too many options (globus_error_match didn't work for me)
        if (error != nil) and !(GFTP.globus_error_print_friendly(error).include? '550 550')
          raise_globus_error(error)
        else
          yield(error == nil)
        end
        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        uri = URI.join(uri, path)
        res = GFTP.globus_ftp_client_chmod(ftphandle, uri.to_s, mode, operationattr, @complete_callback, nil)
        check_error(res) if res != 0
      end
    end

  end
end
