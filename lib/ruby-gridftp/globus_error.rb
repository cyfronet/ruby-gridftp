module GFTP

  # Thrown whenever Ruby-FFI-Globus encounters a problem inside the GridFTP of GSI libraries
  class GlobusError < StandardError
  end

end
