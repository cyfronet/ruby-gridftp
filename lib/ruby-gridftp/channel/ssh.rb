module GFTP
  module Channel
    class Ssh
      def initialize(host:, port: nil, key:, cert:)
        @credentials = GFTP::SshCredentials.new(key, cert)
        @host = host
        @port = port
      end

      def start(&block)
        operationattr = FFI::MemoryPointer.new :pointer
        check_error GFTP.globus_ftp_client_operationattr_init(operationattr)
        check_error GFTP.globus_ftp_client_operationattr_set_authorization(operationattr, nil, username, nil, nil, nil)

        @credentials.with_stored_key_and_cert do |key_path, cert_path|
          yield operationattr, base_uri(File.basename(key_path))
        end

        check_error GFTP.globus_ftp_client_operationattr_destroy(operationattr)
      end

      private
        def username
          @credentials.username
        end

        def base_uri(keyname)
          uri = URI("sshftp://#{username}+++#{keyname}@#{@host}")
          uri.port = @port if @port

          uri
        end

        def check_error(result_code)
          raise_globus_error(GFTP.globus_error_get(result_code)) if result_code != 0
        end

        def raise_globus_error(error)
          raise(GlobusError, GFTP.globus_error_print_friendly(error))
        end
    end
  end
end
