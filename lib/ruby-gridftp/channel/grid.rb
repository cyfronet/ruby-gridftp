module GFTP
  module Channel
    class Grid
      def initialize(host:, port: nil, proxy:)
        @host = host
        @port = port
        @proxy = proxy
      end

      def start(&block)
        operationattr = FFI::MemoryPointer.new :pointer
        check_error GFTP.globus_ftp_client_operationattr_init(operationattr)

        credid = load_proxy(@proxy)
        check_error GFTP.globus_ftp_client_operationattr_set_authorization(operationattr, credid.read_pointer, nil, nil, nil, nil)

        yield operationattr, base_uri

        min_stat = FFI::MemoryPointer.new :pointer
        check_error GFTP.gss_release_cred(min_stat, credid)
        check_error GFTP.globus_ftp_client_operationattr_destroy(operationattr)
      end

      private
        def base_uri
          uri = URI("gsiftp://#{@host}")
          uri.port = @port if @port

          uri
        end

        def load_proxy(proxy_string)
          gss_buf = GFTP::GSSBufferDesc.new
          #gss_buf[:value] = GFTP.globus_common_create_string('X509_USER_PROXY=%s', proxy_path)
          #gss_buf[:value] = GFTP.globus_common_create_string('X509_USER_PROXY=%s', proxy_content)
          gss_buf[:value] = FFI::MemoryPointer.from_string proxy_string
          gss_buf[:length] = gss_buf[:value].read_string.length

          min_stat = FFI::MemoryPointer.new :pointer
          credid = FFI::MemoryPointer.new :pointer
          seconds_to_go = FFI::MemoryPointer.new :pointer

          #p gss_buf[:value].read_string
          #p gss_buf[:length]

          # 0 == GSS_C_NO_OID
          # 0 means passing directly the content of proxy file (for proxy path, use 1 == GSS_IMPEXP_MECH_SPECIFIC)
          # 0 == NULL/0 seconds of validity requested (should result in max proxy validity granted, anyway)
          check_error GFTP.gss_import_cred(min_stat, credid, 0, 0, gss_buf, 0, seconds_to_go)
          check_error min_stat.read_int

          credid
        end

        def check_error(result_code)
          raise_globus_error(GFTP.globus_error_get(result_code)) if result_code != 0
        end

        def raise_globus_error(error)
          raise(GlobusError, GFTP.globus_error_print_friendly(error))
        end
    end
  end
end
