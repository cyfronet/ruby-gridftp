require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  #globus_result_t globus_ftp_client_partial_put	(	globus_ftp_client_handle_t * 	handle,
    #const char * 	url,
    #globus_ftp_client_operationattr_t * 	attr,
    #globus_ftp_client_restart_marker_t * 	restart,
    #globus_off_t 	partial_offset,
    #globus_off_t 	partial_end_offset,
    #globus_ftp_client_complete_callback_t 	complete_callback,
    #void * 	callback_arg
  #)

  class PartialPut < GFTP::Operation

    # Puts a specified _part_ of the given file to GridFTP server, using proxy string for authentication and authorization.
    # User is expected to return an array of two elements: [ data, offset ], from supplied block.
    # Data is a data chunk to write (should not be larger than @buf_size)
    # Offset tells the FTP server where to put the new part of the file (saves one round trip to server to get the current file size).
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The file transfer takes place in asynchronous mode; the passed block is being called ONCE to supply this part of data
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    # In contrary to Put class, this provides ability to "append" an already written file
    def partial_put(dest_path, data, start_offset)
      @data_callback = Proc.new do |user_arg, l_ftphandle, error, buf, length, offset, eof|
        #p 'PartialPut inside own data callback'
        #p "length: #{length}, offset #{offset}, eof #{eof}"
      end

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        raise_globus_error(error) if error != nil
        #p 'FINAL!'
        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        uri = URI.join(uri, path)
        res = GFTP.globus_ftp_client_partial_put(ftphandle, uri.to_s, operationattr, nil, start_offset, start_offset + data.bytes.count, @complete_callback, nil)
        if res == 0
          #supply initial portion of data
          #data, eof = yield(@buf_size)
          @buffer.write_string(data)
          #eof testing requires int as output
          check_error GFTP.globus_ftp_client_register_write(ftphandle, @buffer, data.bytes.count, 0, 1, @data_callback, nil)
        else
          check_error res
        end
      end
    end

  end
end
