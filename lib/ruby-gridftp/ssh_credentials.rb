require "net/ssh"

module GFTP
  class SshCredentials
    def initialize(key, certificate)
      @key = key
      @certificate = certificate
    end

    def valid?
      return false unless @certificate.present?

      public_key.valid_after.past? && public_key.valid_before.future?
    end

    def username
      public_key.key_id
    end

    def with_stored_key_and_cert(&block)
      Tempfile.create() do |key|
        key.write(@key)
        key.flush
        FileUtils.chmod(0600, key)
        key_path = File.absolute_path(key)
        cert = File.open("#{key_path}-cert.pub", "w") do |c|
          c.write(@certificate)
          c.flush
        end

        block.call key_path, File.absolute_path(cert)
      ensure
        File.unlink(cert)
      end
    end

    private
      def public_key
        @public_key = Net::SSH::KeyFactory.load_data_public_key(@certificate)
      end
  end
end
