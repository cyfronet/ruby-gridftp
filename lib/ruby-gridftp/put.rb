require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  #globus_result_t globus_ftp_client_put 	( 	globus_ftp_client_handle_t *  	handle,
		#const char *  	url,
		#globus_ftp_client_operationattr_t *  	attr,
		#globus_ftp_client_restart_marker_t *  	restart,
		#globus_ftp_client_complete_callback_t  	complete_callback,
		#void *  	callback_arg
	#)


  class Put < GFTP::Operation

    # Puts the given file to GridFTP server, using proxy string for authentication and authorization.
    # User is expected to return an array of two elements: [ data, eof ], from supplied block.
    # Data is a data chunk no bigger than @buf_size, and eof is a bool value meaning that there is no more data.
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The file transfer takes place in asynchronous mode; the passed block is being called to supply chunks of data
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    def put(dest_path, &block)
      @data_callback = Proc.new do |user_arg, l_ftphandle, error, buf, length, offset, eof|
        #p 'inside own data callback'
        #p "length: #{length}, offset #{offset}, eof #{eof}"
        if eof == 0
          data, eof = yield(@buf_size)
          buf.write_string(data)
          #p "got data!", buf.read_string
          check_error(GFTP.globus_ftp_client_register_write(l_ftphandle, buf, data.bytes.count, offset + length, eof ? 1 : 0, @data_callback, nil))
        end
      end

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        raise_globus_error(error) if error != nil
        #p @size_counter
        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        uri = URI.join(uri, dest_path)
        res = GFTP.globus_ftp_client_put(ftphandle, uri.to_s, operationattr, nil, @complete_callback, nil)
        if res == 0
          #supply initial portion of data
          data, eof = yield(@buf_size)
          @buffer.write_string(data)
          #eof testing requires int as output
          check_error GFTP.globus_ftp_client_register_write(ftphandle, @buffer, data.bytes.count, 0, eof ? 1 : 0, @data_callback, nil)
        else
          check_error res
        end
      end
    end
  end
end
