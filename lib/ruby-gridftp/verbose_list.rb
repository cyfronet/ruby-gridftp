require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  # globus_result_t globus_ftp_client_verbose_list (
  #   globus_ftp_client_handle_t * u_handle,
  #   const char * url,
  #   globus_ftp_client_operationattr_t * attr,
  #   globus_ftp_client_complete_callback_t complete_callback,
  #   void * callback_arg
  # )


  class VerboseList < GFTP::Operation

    # Gets the verbose list of files in a given path from GridFTP server, using proxy string for authentication and authorization.
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The file list transfer takes place in asynchronous mode; the passed block is being called with the hash of
    # listed data. See complete_callback definition below for the hash structure reference.
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    def verbose_list(path, &block)
      @content = ''

      @data_callback = Proc.new do |user_arg, l_ftphandle, error, buf, length, offset, eof|
        @content += buf.read_string[0..(length-1)]
        check_error(GFTP.globus_ftp_client_register_read(l_ftphandle, buf, @buf_size, @data_callback, nil)) if eof != 1
      end

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        raise_globus_error(error) if error != nil

        yield(@content.lines.select do |line|
            line.split.size > 8
          end.map do |line|
            columns = line.split
            name_start_index = line.index(columns[7]) + 6
            # TODO would create DateTime for the creation_date, if I had only known the year... :)
            {
                rights: columns[0],
                owner: columns[2],
                group: columns[3],
                size: columns[4],
                modification_date: columns[5..7].join(' '),
                name: line[name_start_index..-1].strip,
                is_dir: columns[0][0] == 'd'
            }
        end)

        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        uri = URI.join(uri, path)

        res = GFTP.globus_ftp_client_verbose_list(ftphandle, uri.to_s, operationattr, @complete_callback, nil)
        if res == 0
          check_error GFTP.globus_ftp_client_register_read(ftphandle, @buffer, @buf_size, @data_callback, nil)
        else
          check_error res
        end
      end
    end

  end
end
