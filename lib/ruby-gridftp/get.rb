require_relative 'operation'

module GFTP

  ### Globus Toolkit (5.2.5) function definition ###

  # globus_result_t globus_ftp_client_get	(
  #   globus_ftp_client_handle_t *  handle,
  #   const char *  url,
  #   globus_ftp_client_operationattr_t *  attr,
  #   globus_ftp_client_restart_marker_t *  restart,
  #   globus_ftp_client_complete_callback_t  complete_callback,
  #   void *  callback_arg
  # )

  # TODO - find out what that 'restart' parameter is about (interrupted download continuation??)
  attach_function :globus_ftp_client_get, [:pointer, :pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int


  class Get < GFTP::Operation

    # Gets the given file path from GridFTP server, using proxy string for authentication and authorization.
    # The proxy string should contain entire user's X509 proxy certificate, valid at the moment of function call.
    # The file transfer takes place in asynchronous mode; the passed block is being called with incoming
    # chunks of data.
    # In case of connection, security or transmission problems, a GFTP::GlobusError is raised.
    def get(path, &block)
      @data_callback = Proc.new do |user_arg, l_ftphandle, error, buf, length, offset, eof|
        #p 'inside own data callback'
        #p "length: #{length}, offset #{offset}, eof #{eof}"
        raise_globus_error(error) if error != nil
        @size_counter += length
        yield buf.get_bytes(0, length)
        #p "got data!", buf.read_string
        check_error(GFTP.globus_ftp_client_register_read(l_ftphandle, buf, @buf_size, @data_callback, nil)) if eof != 1
      end

      @complete_callback = Proc.new do |user_arg, l_ftphandle, error|
        raise_globus_error(error) if error != nil
        #p @size_counter
        signal_completion
      end

      run_operation do |ftphandle, operationattr, uri|
        @size_counter = 0
        uri = URI.join(uri, path)
        res = GFTP.globus_ftp_client_get(ftphandle, uri.to_s, operationattr, nil, @complete_callback, nil)
        if res == 0
          check_error GFTP.globus_ftp_client_register_read(ftphandle, @buffer, @buf_size, @data_callback, nil)
        else
          check_error res
        end
      end
    end

  end
end
