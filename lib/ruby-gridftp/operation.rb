require 'ffi'
require_relative 'gftp'
require 'thread'
require_relative 'ssh_credentials'

# The URL of the GridFTP server to contact
#URL = 'gsiftp://qcg.grid.cyf-kr.edu.pl/'
#URL = 'gsiftp://zeus.cyfronet.pl/'
#URL = 'gsiftp://dpm.cyf-kr.edu.pl:8446/dpm/cyf-kr.edu.pl/people/ymgubala/'

# The memory buffer in bytes reserved for reading server response
#BUF_SIZE = 1024*1024
#TODO: convert buf_size to a variable that can be set by user at the initialization of lib
#BUF_SIZE = 4096
#1MB
BUF_SIZE = 1024*1024

# If false, the library expects server paths to be absolute; else, they should be relative (to user's home)
REL_PATHS = false

# We need native glibc memory allocation to have read buffer not GCed by MRI Ruby
module LibC
  extend FFI::Library
  ffi_lib FFI::Library::LIBC

  attach_function :malloc, [:size_t], :pointer
  attach_function :free, [:pointer], :void
end


module GFTP
  #GFTP.globus_thread_set_model('pthread')
  GFTP.globus_thread_set_model('none')
  GFTP.globus_module_activate(GFTP.globus_i_ftp_client_module)

  class Operation
    def initialize(preferred_buf_size = BUF_SIZE, channel)
      # TODO FIXME check for any possible allocation problem
      @buf_size = preferred_buf_size
      @buffer = LibC.malloc @buf_size
      @channel = channel
    end

    private
      attr_reader :channel

      def run_operation(&block)
        ftphandle = FFI::MemoryPointer.new :pointer
        ftphandleattr = FFI::MemoryPointer.new :pointer

        @mutex = Mutex.new
        @done = false

        check_error GFTP.globus_ftp_client_handleattr_init(ftphandleattr)
        check_error GFTP.globus_ftp_client_handleattr_set_rfc1738_url(ftphandleattr, REL_PATHS ? 1 : 0)
        check_error GFTP.globus_ftp_client_handle_init(ftphandle, ftphandleattr)

        @channel.start do |operationattr, base_uri|
          yield ftphandle, operationattr, base_uri

          catch :complete do
            begin
              #this needs to be called periodically, if not background threads will not be processed
              GFTP.globus_callback_space_poll(GFTP.globus_i_abstime_zero, -2)
              @mutex.synchronize do
                throw :complete if @done
              end
            end while true
            puts 'done waiting for cond!'
          end
        end

        # NOTE: 'operationattr' taken care of in the wrapper method 'authorize_with_proxy'

        check_error GFTP.globus_ftp_client_handleattr_destroy(ftphandleattr)
        check_error GFTP.globus_ftp_client_handle_destroy(ftphandle)

        # NOTE: we NEED to comment that out, otherwise OpenSSL is being strangely mangled by Globus :(
        #check_error GFTP.globus_module_deactivate(GFTP.globus_i_ftp_client_module)
      end

      def check_error(result_code)
        raise_globus_error(GFTP.globus_error_get(result_code)) if result_code != 0
      end

      def raise_globus_error(error)
        raise(GlobusError, GFTP.globus_error_print_friendly(error))
      end

      # NOTE! All read @buffer data should already be copied-away - be dealloc native memory here
      def signal_completion
        #This seems optional, but we'll leave it here just in case
        GFTP.globus_thread_blocking_space_will_block(-2)
        LibC.free @buffer
        @mutex.synchronize do
          @done = true
        end
      end
  end
end
