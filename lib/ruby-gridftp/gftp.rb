module GFTP
	extend FFI::Library
#todo: somehow make this a relative path
  # TG: for now added alternative paths; they should work as fallbacks
	ffi_lib ['/home/yaq/glo/lib64/libglobus_ftp_client.so', '/home/gubala/rubycon/lifescience/globus/build/lib64/libglobus_ftp_client.so.2', 'libglobus_ftp_client.so.2', 'libglobus_ftp_client.so.1']

	class GlobusModuleId < FFI::Struct
		layout :module_name, :string,
			:act, :pointer,
			:deact, :pointer,
			:atexit, :pointer,
      :getpointer, :pointer,
			:version, :pointer,
			:errfunc, :pointer
  end

  class GSSBufferDesc < FFI::Struct
    layout :length, :size_t,
           :value, :pointer
  end

  class GlobusAbstime < FFI::Struct
    layout :tv_sec, :long,
           :tv_nsec, :long
  end

  #error reading
  attach_function :globus_object_printable_to_string, [:pointer], :pointer
  attach_function :globus_error_print_friendly, [:pointer], :string
  attach_function :globus_error_get, [:int], :pointer

  #module initialization
	attach_variable :globus_i_ftp_client_module, GlobusModuleId

	attach_function :globus_module_activate, [:pointer], :int
  attach_function :globus_module_deactivate, [:pointer], :int

  #handle
  attach_function :globus_ftp_client_handle_init, [:pointer, :pointer], :int
  attach_function :globus_ftp_client_handle_destroy, [:pointer], :int
  #handleattr
  attach_function :globus_ftp_client_handleattr_init, [:pointer], :int
  attach_function :globus_ftp_client_handleattr_destroy, [:pointer], :int
  #enable relative file paths
  attach_function :globus_ftp_client_handleattr_set_rfc1738_url, [:pointer, :int], :int

  #operation attr
  attach_function :globus_ftp_client_operationattr_init, [:pointer], :int
  attach_function :globus_ftp_client_operationattr_destroy, [:pointer], :int
  attach_function :globus_ftp_client_operationattr_set_authorization, [:pointer, :pointer, :pointer, :pointer, :pointer, :pointer, ], :int

  #done callback
  callback :globus_ftp_client_complete_callback_t, [:pointer, :pointer, :pointer], :void
  #operations
  #exists operation
  attach_function :globus_ftp_client_exists, [:pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #verbose_list operation
  attach_function :globus_ftp_client_verbose_list, [:pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #get operation
  attach_function :globus_ftp_client_get, [:pointer, :pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #put operation
  attach_function :globus_ftp_client_put, [:pointer, :pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #partial_put operation
  attach_function :globus_ftp_client_partial_put, [:pointer, :pointer, :pointer, :pointer, :int64, :int64, :globus_ftp_client_complete_callback_t, :pointer], :int
  #delete operation
  attach_function :globus_ftp_client_delete, [:pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #mkdir operation
  attach_function :globus_ftp_client_mkdir, [:pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #rmdir operation
  attach_function :globus_ftp_client_rmdir, [:pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #chmod operation
  attach_function :globus_ftp_client_chmod, [:pointer, :pointer, :int, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #move operation
  attach_function :globus_ftp_client_move, [:pointer, :pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int
  #cp operation
  attach_function :globus_ftp_client_third_party_transfer, [:pointer, :pointer, :pointer, :pointer, :pointer, :pointer, :globus_ftp_client_complete_callback_t, :pointer], :int

  #data callback
  callback :globus_ftp_client_data_callback_t, [:pointer, :pointer, :pointer, :pointer, :int, :long, :int], :void
  #register read function
  attach_function :globus_ftp_client_register_read, [:pointer, :pointer, :int, :globus_ftp_client_data_callback_t, :pointer], :int
  #register write function
  attach_function :globus_ftp_client_register_write, [:pointer, :pointer, :int, :int64, :int, :globus_ftp_client_data_callback_t, :pointer], :int

  #GSI
  #GSS_C_NO_OID to (gss_OID)0, gss_OID to *gss_OID_set_desc
  attach_function :gss_import_cred, [:pointer, :pointer, :int, :int, GSSBufferDesc, :int, :pointer], :int
  attach_function :gss_export_cred, [:pointer, :pointer, :int, :int, GSSBufferDesc], :int
  attach_function :gss_release_cred, [:pointer, :pointer], :int

  #globus_threads

  #threading initialization
  attach_function :globus_thread_set_model, [:string], :int
  #parameter should be always -2 (GLOBUS_CALLBACK_GLOBAL_SPACE)
  attach_function :globus_thread_blocking_space_will_block, [:int], :int
  attach_variable :globus_i_abstime_zero, GlobusAbstime
  #parameters should be always :globus_i_abstime_zero , -2 (GLOBUS_CALLBACK_GLOBAL_SPACE)
  attach_function :globus_callback_space_poll, [:pointer, :int], :int

  #mutex operations
  attach_function :globus_mutex_init, [:pointer, :pointer], :int
  attach_function :globus_mutex_lock, [:pointer], :int
  attach_function :globus_mutex_unlock, [:pointer], :int
  #cond operations
  attach_function :globus_cond_init, [:pointer, :pointer], :int
  attach_function :globus_cond_signal, [:pointer], :int
  attach_function :globus_cond_wait, [:pointer, :pointer], :int

  #helpers
  attach_function :globus_common_create_string, [:string, :string], :pointer
end