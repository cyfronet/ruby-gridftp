require_relative 'delete'
require_relative 'exists'
require_relative 'get'
require_relative 'mkdir'
require_relative 'put'
require_relative 'verbose_list'
require_relative 'transfer'

module GFTP
  #
  # GridFTP client.
  #
  class Client

    #
    # Creates grid ftp client.
    #
    # The proxy string should contain entire user's X509 proxy
    # certificate, valid at the moment of function call.
    #
    # It is possible to define the buffer size. If this value is not
    # set than default one (BUF_SIZE) is used.
    #
    def initialize(channel, preferred_buf_size = BUF_SIZE)
      @preferred_buf_size = preferred_buf_size

      @channel = channel
    end

    #
    # Removes a file with the given path (not directories!) from the
    # GridFTP server, using proxy string for authentication and authorization.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the delete operation..
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def delete(path, &block)
      Delete.new(@preferred_buf_size, @channel).delete(path, &block)
    end

    #
    # Checks if a file or a directory with the given path exists on GridFTP
    # server, using proxy string for authentication and authorization.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the existence check. Please note
    # that due to file systems nature, checking existence of a certain path
    # does not give the definite answer whether the given path is to regular
    # file, a link or a directory.
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def exists(path, &block)
      Exists.new(@preferred_buf_size, @channel).exists(path, &block)
    end

    #
    # Gets the given file path from GridFTP server, using proxy string
    # for authentication and authorization.
    #
    # The file transfer takes place in asynchronous mode;
    # the passed block is being called with incoming chunks of data.
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def get(path, &block)
      Get.new(@preferred_buf_size, @channel).get(path, &block)
    end

    #
    # Creates a directory with the given name, at the given location on
    # GridFTP server, using proxy string for authentication and authorization.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the mkdir operation..
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def mkdir(path, &block)
      MkDir.new(@preferred_buf_size, @channel).mkdir(path, &block)
    end

    #
    # Equivalent of bash 'mkdir -p' command: no error if existing,
    # make parent directories as needed.
    #
    # path - directory path to be created
    # block - block to be executed with true if dir is created
    #         successfully, false otherwise
    #
    def mkdir!(path, &block)
      elements = path.split('/')
      if path.start_with?('gsiftp')
        start_index = 3
        stub = elements[0, 3].join('/') + '/'
      else
        start_index = 1
        stub = '/'
      end
      begin
        elements[start_index, elements.length].inject(stub) do |p, el|
          path = "#{p}#{el}/"
          exists path do |exists|
            mkdir path do |created|
              raise unless created
            end unless exists
          end
          path
        end
        yield true
      rescue
        yield false
      end
    end

    #
    # Removes a directory with the given name, at the given location on
    # GridFTP server.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the rmdir operation.
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def rmdir(path, &block)
        RmDir.new(@preferred_buf_size, @channel).rmdir(path, &block)
    end

    #
    # Removes a directory with the given name, at the given location on
    # GridFTP server.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the rmdir operation.
    #
    # It does not raise exception even if the directory was not existing.
    #
    def rmdir!(path, &block)
        RmDir.new(@preferred_buf_size, @channel).rmdir!(path, &block)
    end

    #
    # Puts the given file to GridFTP server, using proxy string for
    # authentication and authorization. User is expected to return an
    # array of two elements: [ data, eof ], from supplied block. Data
    # is a data chunk no bigger than BUF_SIZE, and eof is a bool value
    # meaning that there is no more data.
    #
    # The file transfer takes place in asynchronous mode; the passed
    # block is being called to supply chunks of data
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def put(dest_path, &block)
      Put.new(@preferred_buf_size, @channel).put(dest_path, &block)
    end

    #
    # Moves/renames the source_path folder or file into destination_path
    # folder or file.
    #
    # The operation is asynchronous; the passed block is being called
    # with either true or false as the result of the move operation..
    #
    # In case of connection, security or transmission problems, a
    # GFTP::GlobusError is raised.
    #
    def move(source_path, desctination_path, &block)
      Move.new(@preferred_buf_size, @channel).move(source_path, desctination_path, &block)
    end

    #
    # Transfers the file located in source_path to destination path.
    # Can also rename in the process
    #
    # In case of connection, security or transmission problems, a
    # GFTP::GlobusError is raised.
    #
    def transfer(source_path, destination_path, &block)
      Transfer.new(@preferred_buf_size, @channel).transfer(source_path, destination_path, &block)
    end

    #
    # Puts a specified _part_ of the given file to GridFTP server,
    # using proxy string for authentication and authorization.
    #
    # User is expected to return an array of two elements:
    # [ data, offset ], from supplied block.
    #
    # Data is a data chunk to write (should not be larger than @buf_size)
    #
    # Offset tells the FTP server where to put the new part of the file
    # (saves one round trip to server to get the current file size).
    #
    # The file transfer takes place in asynchronous mode;
    # the passed block is being called ONCE to supply this part of data
    #
    # In case of connection, security or transmission problems, a
    # GFTP::GlobusError is raised.
    #
    # In contrary to Put class, this provides ability to "append" an
    #  already written file
    #
    def partial_put(dest_path, data, start_offset)
      PartialPut.new(@preferred_buf_size, @channel).partial_put(dest_path, data, start_offset)
    end

    #
    # Gets the verbose list of files in a given path from GridFTP server,
    # using proxy string for authentication and authorization.
    #
    # The file list transfer takes place in asynchronous mode; the passed
    # block is being called with the hash of listed data.
    #
    # In case of connection, security or transmission problems,
    # a GFTP::GlobusError is raised.
    #
    def verbose_list(path, &block)
      VerboseList.new(@preferred_buf_size, @channel).verbose_list(path, &block)
    end

    #
    # Changes the path folder or file access attributes according to the mode
    # octal number.
    #
    # The operation is asynchronous; the passed block is being called with
    # either true or false as the result of the chmod operation..
    #
    # In case of connection, security or transmission problems, a
    # GFTP::GlobusError is raised.
    #
    def chmod(path, mode, &block)
      Chmod.new(@preferred_buf_size, @channel).chmod(path, mode, &block)
    end
  end
end
