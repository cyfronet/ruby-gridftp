#include <stdlib.h>
#include <stdio.h>

#include "globus_gsi_credential.h"
#include "globus_ftp_client.h"

typedef struct {
	globus_mutex_t mutex;
	globus_cond_t cond;
	volatile globus_bool_t done;
} monitor_t;

void check_result(globus_result_t result) {
	if(result == GLOBUS_SUCCESS) {
		printf("success\n");
	} else {
		printf("fail\n");
		printf("error: %s", globus_object_printable_to_string(globus_error_get(result)));
		exit(1);
	}
}

void callb(void *v_monitor, globus_ftp_client_handle_t *handle, globus_object_t *error) {
	monitor_t *monitor = (monitor_t*) v_monitor;
	globus_mutex_lock(&monitor->mutex);

	printf("error: %s", globus_object_printable_to_string(error));
	printf("operation finished\n");

	monitor->done = GLOBUS_TRUE;
	globus_cond_signal(&monitor->cond);
	globus_mutex_unlock(&monitor->mutex);
}

void datacallb(void *v_monitor, globus_ftp_client_handle_t *handle, globus_object_t *error, globus_byte_t *buf, globus_size_t bufsize, globus_off_t offset, globus_bool_t eof) {
	//monitor_t *monitor = (monitor_t*) v_monitor;
	printf("offset: %d\n", (int) offset);
	printf("eof: %d\n", eof);
}


int read_file(char* file_name, char* proxy_file_name, globus_ftp_client_data_callback_t callback) {
	globus_ftp_client_handle_t ftphandle;
	globus_ftp_client_handleattr_t ftphandleattr;
	globus_ftp_client_operationattr_t operattr;
	globus_result_t result;
	monitor_t monitor;

	//proxy shit
    OM_uint32 maj_stat;
    OM_uint32 min_stat;
    gss_cred_id_t credid;
    gss_buffer_desc gssbuf;

	static int BUF_SIZE = 10;
	char buf[BUF_SIZE];

	globus_mutex_init(&monitor.mutex, NULL);
	globus_cond_init(&monitor.cond, NULL);
	monitor.done = GLOBUS_FALSE;

	char url[] = "gsiftp://qcg.grid.cyf-kr.edu.pl/";

	//create handleattrs
	result = globus_ftp_client_handleattr_init(&ftphandleattr);
	check_result(result);
	result = globus_ftp_client_handleattr_set_rfc1738_url(&ftphandleattr, GLOBUS_TRUE);
	check_result(result);


	result = globus_module_activate(GLOBUS_FTP_CLIENT_MODULE);
	check_result(result);
	result = globus_ftp_client_handle_init(&ftphandle, &ftphandleattr);
	check_result(result);

	result = globus_ftp_client_operationattr_init(&operattr);
	check_result(result);

	//load proxy
	//gssbuf.value = globus_common_create_string("X509_USER_PROXY=%s", "/home/yaq/x509up_u20271");
	gssbuf.value = globus_common_create_string("X509_USER_PROXY=%s", proxy_file_name);
	gssbuf.length = strlen(gssbuf.value);
	maj_stat = gss_import_cred(&min_stat, &credid, GSS_C_NO_OID, 1 /* GSS_IMPEXP_MECH_SPECIFIC */, &gssbuf, 0, NULL);
	printf("maj_stat: %s\n", maj_stat==GSS_S_COMPLETE ? "success" : "failed");
	printf("min_stat: %s\n", min_stat==GSS_S_COMPLETE ? "success" : "failed");
	globus_free(gssbuf.value);

	//set custom authorization
	result = globus_ftp_client_operationattr_set_authorization(&operattr, credid, NULL, NULL, NULL, NULL);
	check_result(result);

	result = globus_ftp_client_get(&ftphandle, strcat(url, file_name), &operattr, NULL, callb, &monitor);
	check_result(result);

	result = globus_ftp_client_register_read(&ftphandle, (globus_byte_t*)buf, BUF_SIZE, callback, &monitor);
	check_result(result);

	globus_mutex_lock(&monitor.mutex);
	while(!monitor.done) {
		globus_cond_wait(&monitor.cond, &monitor.mutex);
	}
	globus_mutex_unlock(&monitor.mutex);

	globus_ftp_client_handle_destroy(&ftphandle);
	check_result(result);

//	printf("output:\n%s\nend of output\n", buf);
//	printf("strlen: %d\n", (int)strlen(buf));
}


int main() {
	globus_ftp_client_handle_t ftphandle;
	globus_ftp_client_handleattr_t ftphandleattr;
	globus_ftp_client_operationattr_t operattr;
	globus_result_t result;
	monitor_t monitor;

	//proxy shit
    OM_uint32 maj_stat;
    OM_uint32 min_stat;
    gss_cred_id_t credid;
    gss_buffer_desc gssbuf;

	static int BUF_SIZE = 100000;
	char buf[BUF_SIZE];

	globus_mutex_init(&monitor.mutex, NULL);
	globus_cond_init(&monitor.cond, NULL);
	monitor.done = GLOBUS_FALSE;

	char url[] = "gsiftp://qcg.grid.cyf-kr.edu.pl/";

	//create handleattrs
	result = globus_ftp_client_handleattr_init(&ftphandleattr);
	check_result(result);
	result = globus_ftp_client_handleattr_set_rfc1738_url(&ftphandleattr, GLOBUS_TRUE);
	check_result(result);


	result = globus_module_activate(GLOBUS_FTP_CLIENT_MODULE);
	check_result(result);
	result = globus_ftp_client_handle_init(&ftphandle, &ftphandleattr);
	check_result(result);

	result = globus_ftp_client_operationattr_init(&operattr);
	check_result(result);

	//load proxy
	gssbuf.value = globus_common_create_string("X509_USER_PROXY=%s", "/home/yaq/x509up_u20271");
	//gssbuf.value = globus_common_create_string("X509_USER_PROXY=%s", "/tmp/x509up_u1000");
	gssbuf.length = strlen(gssbuf.value);
	maj_stat = gss_import_cred(&min_stat, &credid, GSS_C_NO_OID, 1 /* GSS_IMPEXP_MECH_SPECIFIC */, &gssbuf, 0, NULL);
	printf("maj_stat: %s\n", maj_stat==GSS_S_COMPLETE ? "success" : "failed");
	printf("min_stat: %s\n", min_stat==GSS_S_COMPLETE ? "success" : "failed");
	globus_free(gssbuf.value);

	//set custom authorization
	result = globus_ftp_client_operationattr_set_authorization(&operattr, credid, NULL, NULL, NULL, NULL);
	// result = globus_ftp_client_operationattr_set_authorization(&operattr, GSS_C_NO_CREDENTIAL, NULL, NULL, NULL, NULL);
	check_result(result);

	result = globus_ftp_client_verbose_list(&ftphandle, url, &operattr, callb, &monitor);
	check_result(result);

	result = globus_ftp_client_register_read(&ftphandle, (globus_byte_t*)buf, BUF_SIZE, datacallb, &monitor);
	check_result(result);

	globus_mutex_lock(&monitor.mutex);
	while(!monitor.done) {
		globus_cond_wait(&monitor.cond, &monitor.mutex);
	}
	globus_mutex_unlock(&monitor.mutex);


	globus_ftp_client_handle_destroy(&ftphandle);
	check_result(result);

	printf("output:\n%s\nend of output\n", buf);
	printf("strlen: %d\n", (int)strlen(buf));

	return 0;
}
