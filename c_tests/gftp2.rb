require 'rubygems'
require 'ffi'

module Gftp2
  extend FFI::Library
  ffi_lib 'gftp.so'

  callback :globus_ftp_client_data_callback_t, [:pointer, :pointer, :pointer, :pointer, :int, :int, :bool], :void
  attach_function :read_file, [:string, :string, :globus_ftp_client_data_callback_t], :void
end

Gftp2::DataCallback = Proc.new do |user_arg, handle, error, buffer, length, offset, eof|
  p 'Inside DataCallback'
  p buffer.read_string
end

Gftp2.read_file 'scr.sh', "/tmp/x509up_u#{Process.uid}", Gftp2::DataCallback
